import {
  HoneylemonService,
  COLLATERAL_TOKEN_DECIMALS,
  PAYMENT_TOKEN_DECIMALS,
  POSITIONS_QUERY,
  CONTRACTS_QUERY,
  CONTRACT_DURATION
} from './HoneylemonService';

export {
  HoneylemonService,
  COLLATERAL_TOKEN_DECIMALS,
  PAYMENT_TOKEN_DECIMALS,
  POSITIONS_QUERY,
  CONTRACTS_QUERY,
  CONTRACT_DURATION
};
